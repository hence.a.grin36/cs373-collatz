#!/usr/bin/env python3

# pylint: disable = missing-docstring

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read,                           \
    collatz_eval,                           \
    collatz_print,                          \
    collatz_solve, compute_cycle_length,    \
    compute_max_cycle_length,               \
    meta_cache_max_cycle_length

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        read_in = "1 10\n"
        i, j = collatz_read(read_in)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_0(self):
        value = collatz_eval(500, 2500)
        self.assertEqual(value, 209)

    def test_eval_1(self):
        value = collatz_eval(1, 3)
        self.assertEqual(value, 8)

    def test_eval_2(self):
        value = collatz_eval(100, 200)
        self.assertEqual(value, 125)

    def test_eval_3(self):
        value = collatz_eval(201, 210)
        self.assertEqual(value, 89)

    def test_eval_4(self):
        value = collatz_eval(1000, 900)
        self.assertEqual(value, 174)

    def test_eval_5(self):
        value = collatz_eval(1, 1000000)
        self.assertEqual(value, 525)

    # ----
    # print
    # ----

    def test_print(self):
        write = StringIO()
        collatz_print(write, 1, 10, 20)
        self.assertEqual(write.getvalue(), "1 10 20\n")

    # ----
    # solve
    # ----

    def test_solve(self):
        read = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        write = StringIO()
        collatz_solve(read, write)
        self.assertEqual(
            write.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    # ----
    # compute_cycle_length
    # ----

    def test_compute_cycle_length_0(self):
        value = compute_cycle_length(1)
        self.assertEqual(value, 1)

    def test_compute_cycle_length_1(self):
        value = compute_cycle_length(3)
        self.assertEqual(value, 8)

    def test_compute_cycle_length_2(self):
        value = compute_cycle_length(837799)
        self.assertEqual(value, 525)

    # ----
    # compute_max_cycle_length
    # ----

    def test_compute_max_cycle_length_0(self):
        value = compute_max_cycle_length(1, 3)
        self.assertEqual(value, 8)

    def test_compute_max_cycle_length_1(self):
        value = compute_max_cycle_length(100, 200)
        self.assertEqual(value, 125)

    def test_compute_max_cycle_length_2(self):
        value = compute_max_cycle_length(201, 210)
        self.assertEqual(value, 89)

    def test_compute_max_cycle_length_3(self):
        value = compute_max_cycle_length(1, 1000000)
        self.assertEqual(value, 525)

    # ----
    # compute_max_cycle_length
    # ----

    def test_meta_cache_0(self):
        value = meta_cache_max_cycle_length(1, 999)
        self.assertEqual(value, 0)

    def test_meta_cache_1(self):
        value = meta_cache_max_cycle_length(999000, 1000000)
        self.assertEqual(value, 396)

    def test_meta_cache_2(self):
        value = meta_cache_max_cycle_length(45176, 893285)
        self.assertEqual(value, 525)

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
