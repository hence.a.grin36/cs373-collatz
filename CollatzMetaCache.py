#!/usr/bin/env python3

# from sys import stdin, stdout
from typing import IO, List

# ------------
# collatz_calc
# ------------

CACHE_SIZE = 1000001
cache = [0] * CACHE_SIZE

def collatz_calc (n: int) -> int:
    assert n > 0

    cycle_length = 0

    # Check if cycle length of n has been cached
    if n < CACHE_SIZE and cache[n] != 0:
        cycle_length += cache[n]

    # Recursively compute cycle length of n
    else:
        # Base case
        if n == 1:
            cycle_length = 1

        # n is even
        elif (n % 2) == 0:
            cycle_length = 1 + collatz_calc(n // 2)

        # n is odd
        else:
            cycle_length = 2 + collatz_calc(n + (n >> 1) + 1)

        # Store computed cycle length of n in cache
        if n < CACHE_SIZE and cache[n] == 0:
            cache[n] = cycle_length

    assert cycle_length > 0
    return cycle_length


# ------------
# collatz_eval
# ------------

def collatz_eval (i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """

    assert i > 0 and i <= 1000000
    assert j > 0 and j <= 1000000

    # Swap values of i and j if range is given backwards
    if i <= j:
        start = i
        end = j
    else:
        start = j
        end = i

    mid = end // 2 + 1
    if i < mid:
        start = mid

    assert start <= end

    max_cycle_length = 0

    for n in range(start, end + 1):
        cycle_length = collatz_calc(n)
        assert cycle_length == cache[n]

        max_cycle_length = max(cycle_length, max_cycle_length)
        assert max_cycle_length >= cycle_length

    # print(sorted(cache.items()))

    assert max_cycle_length > 0
    return max_cycle_length

# -------------
# collatz_print
# -------------

META_CACHE = [0] * 1000

def print_meta_cache () -> None:
	meta_cache = "["
	for n in range(0, 1000):
		meta_cache += str(META_CACHE[n])
		if (n % 13) == 12:
			meta_cache += ",\n"
		else:
			meta_cache += ", "
	meta_cache += "]"
	print(meta_cache)


def build_meta_cache () -> None:
	for n in range(1, 1000001, 1000):
		META_CACHE[n // 1000] = collatz_eval(n, n + 999)

# -------------
# collatz_solve
# -------------

if __name__ == "__main__":
	build_meta_cache()
	print_meta_cache()
