#!/usr/bin/env python3

# ----------
# BuildAcceptanceTests.py
# ----------

import random

def generate_acceptance_tests() -> None:
	file = open("collatz-tests/hence.a.grin36-RunCollatz.in", "w")

	for i in range(1, 201):
		a = str(random.randint(1, 1000001))
		b = str(random.randint(1, 1000001))
		
		if a <= b:
			res = a + " " + b + "\n"
		else:
			res = b + " " + a + "\n"

		file.write(res)

	file.close()

if __name__ == "__main__":
	generate_acceptance_tests()